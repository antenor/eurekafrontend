import Vue from 'vue'
import Router from 'vue-router'

import MainPage from '@/components/pages/MainPage'
import DetailPage from '@/components/pages/DetailPage'
import StartPage from '@/components/pages/StartPage'
import PlaceAds from '@/components/pages/PlaceAds'

Vue.use(Router)
Vue.filter('trans', (key) => {
    return "translation of " + key;
});

export default new Router({
    routes: [
        {
            path: '/',
            name: 'MainPage',
            component: MainPage
        },
        {
            path: '/intro',
            props: true,
            name: "StartPage",
            component: StartPage
        },
        {
            path: '/plaats',
            name: 'PlaceAds',
            component: PlaceAds
        },
        {
            path: '/:product',
            props: true,
            name: "DetailPage",
            component: DetailPage
        }
    ]
});
