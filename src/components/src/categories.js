var autos = [" Alfa romeo ", " Audi ", " BMW ", " Chevrolet ", " Citroën ", " Fiat ", " Ford ", " Honda ", " Hyundai ", " Land Rover ", " Mercedes-Benz ", " MINI ", " Nissan ", " Opel ", " Peugeot ", " Porsche ", " Renault ", " Seat ", " Toyota ", " Volkswagen ", " Volvo "];
var carparts = ["Aanhangwagens", "Audio / Radio", "Banden", "Beveiliging", "Boxen & versterkers", "Bumpers", "Dakdragers", "Fietsendragers", "Gereedschap", "GPS", "Hoezen & matten", "Onderdelen", "Onderhoud / Handleidingen", "Schadewagens", "Sneeuwkettingen", "Stuur", "Trekhaken", "Tuning en autostyling", "Uitlaten", "Velgen", "Verlichting", "Zetels", "Andere accessoires"];
var motors = ["Motoren", "Scooters", "Brommers", "Quads/trikes"];
var motorsparts = ["Motorkleding", "Helmen", "Motoronderdelen", "Overige"];
var companycars = ["Bestelwagens", "Vrachtwagens"];
var boatsandparts = ["Motorboten", "Zeilboten", "Overige boten", "Bootonderdelen"];
var caravancamps = ["Tenten", "Caravans", "Motorhomes", "Kampeeraccessoires"];

//Immo
var houseSale = ["Huizen", "Appartementen", "Lofts", "Gronden", "Garages", "Bedrijfspanden"];
var houseRent = ["Huizen", "Appartementen", "Lofts", "Gronden", "Garages", "Bedrijfspanden", "Kot en samenhuizen"];
var houseVacation = ["België", "Europa", "Overige"];

//Multimedia
var computers = ["Software", "Desktops", "Laptops en tablets", "Printers en accessoires", "Computeronderdelen", "Overige"];
var gamesconsole  = ["Switch", "PlayStation", "Xbox", "PC", "Handhelds", "Overige"];
var filmsTv = ["Beamers en projectoren", "Blu-ray spelers", "Dvd-spelers", "Films", "Home cinema", "Satelliet schotels en receivers", "Tv's", "Tv accessoires", "Videorecorders", "Overige tv"];
var musicAudios = ["Bekabeling", "Cassettebandjes", "Cd's", "Cd-spelers en bandrecorders", "Koptelefoons en oortjes", "Lichteffecten", "Luidsprekers en boxen", "Mengpanelen", "Microfoons", "MP3-spelers en iPod", "Platenspelers", "Radio's en stereo's", "Versterkers en receivers", "Vinylplaten", "Overige muziek en audio"];
var photosCameras = ["Accu's, batterijen en opladers", "Analoge camera's", "Cameratassen", "Digitale camera's", "Digitale fotolijsten", "Flitsers", "Geheugenkaartjes", "Lenzen en filters", "Microscopen", "Spiegelreflexcamera's", "Statieven", "Telescopen", "Verrekijkers", "Videobewaking", "Videocamera's", "Overige fotografie"];
var phones = ["GSM / Telefoons", "Telefonie accessoires"];



let categoriesItems = [ 
    { title: "Auto's", subs: autos },
    { title: "Auto's: Onderdelen",  subs: carparts },
    { title: "Motoren", subs: motors },
    { title: "Motoren: Onderdelen", subs: motorsparts },
    { title: "Bedrijfsvoertuigen", subs: companycars },
    { title: "Boten en onderdelen", subs: boatsandparts },
    { title: "Caravans en kamping", subs: caravancamps },
    { title: "Immo - Te Koop", subs: houseSale },
    { title: "Immo - Te Huur", subs: houseRent },
    { title: "Immo - Vakantiehuizen", subs: houseVacation },
    { title: "Computers", subs: computers },
    { title: "Games en Consoles", subs: gamesconsole },
    { title: "Films en TV", subs: filmsTv },
    { title: "Muziek en audio", subs: musicAudios },
    { title: "Fotografie en camera's", subs: photosCameras },
    { title: "Telefonie", subs: phones }
];

let categories = [
    { title: "Auto's" },
    { title: "Auto's: Onderdelen"},
    { title: "Motoren"},
    { title: "Motoren: Onderdelen" },
    { title: "Bedrijfsvoertuigen" },
    { title: "Boten en onderdelen"  },
    { title: "Caravans en kamping" },
    { title: "Immo - Te Koop" },
    { title: "Immo - Te Huur" },
    { title: "Immo - Vakantiehuizen" },
    { title: "Computers" },
    { title: "Games en Consoles" },
    { title: "Films en TV"  },
    { title: "Muziek en audio" },
    { title: "Fotografie en camera's" },
    { title: "Telefonie" }
]

export class Categories {
    getList() { return categoriesItems }
    getCategories() { return categories }
}